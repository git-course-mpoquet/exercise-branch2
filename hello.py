#!/usr/bin/env python3
'''Just a hello world script.'''

def hail(stuff: str) -> None:
    '''Hello world function.

    Parameters
    ----------
    stuff : str
            The stuff to hail.
    '''
    print(f'Hello, {stuff}!')

hail('world')
